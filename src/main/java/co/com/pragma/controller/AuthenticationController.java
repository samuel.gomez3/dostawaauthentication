package co.com.pragma.controller;


import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import co.com.pragma.dto.UserRq;

import co.com.pragma.services.IAuthenticationService;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class AuthenticationController {
	
	
	private final IAuthenticationService authenticationService ;


    @PostMapping("/authenticate")
    public Object createAuthenticationToken(@RequestBody UserRq userRq) throws Exception {

        return authenticationService.authentication(userRq);
    }
    @PostMapping("/createUser")
    public Object createUser(@RequestBody UserRq userRq) throws Exception {
    	
    	return authenticationService.guardarUsuario(userRq);
    }
    @PostMapping("/validar")
    public Object validar(@RequestBody String token) throws Exception {
    	
    	return authenticationService.validarToken(token);
    }

}
