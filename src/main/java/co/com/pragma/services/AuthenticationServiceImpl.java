package co.com.pragma.services;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import co.com.pragma.dto.UserRq;
import co.com.pragma.entities.Role;
import co.com.pragma.entities.User;
import co.com.pragma.repositories.RoleRepository;
import co.com.pragma.repositories.UserRepository;
import co.com.pragma.util.BCryptUtil;
import co.com.pragma.util.JwtUtil;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor

@Service
public class AuthenticationServiceImpl implements IAuthenticationService {

	private final UserRepository userRepository;
	private final RoleRepository roleRepository;

	public Object guardarUsuario(UserRq userRq) {

		Role role = roleRepository.findById(userRq.getRole()).get();

		return userRepository.save(new User(0L, userRq.getUsername(), BCryptUtil.hashPassword(userRq.getPassword()),
				role, userRq.getFirstName(), userRq.getLastName(), userRq.getEmail()));
	}
	
	
	public Object authentication(UserRq userRq) {
		String userName = userRq.getUsername();
		User user = userRepository.findByUsername(userName);
		
		if (!BCryptUtil.checkPass(userRq.getPassword(), user.getPassword())) {
			return "Unauthorised";
		}
		
	       Map<String, Object> claims = new HashMap<>();
	        claims.put("email", user.getEmail());
	        claims.put("roles", user.getRole().getName());

	        String token = JwtUtil.generateToken(userName, claims);

	        return token;
	}
	
	
	public Object validarToken(String token) {

		return JwtUtil.validateToken(token);
	}
	

}
