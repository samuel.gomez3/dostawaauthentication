package co.com.pragma.services;

import co.com.pragma.dto.UserRq;

public interface IAuthenticationService {

	public Object guardarUsuario(UserRq userRq);

	public Object authentication(UserRq userRq);

	public Object validarToken(String token);
}
