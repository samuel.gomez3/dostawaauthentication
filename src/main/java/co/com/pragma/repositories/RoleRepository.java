package co.com.pragma.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.pragma.entities.Role;


public interface RoleRepository extends JpaRepository<Role, Long> {

}
