package co.com.pragma.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.pragma.entities.User;

public interface UserRepository extends JpaRepository<User, Long> {
	User findByUsername(String username);
}
