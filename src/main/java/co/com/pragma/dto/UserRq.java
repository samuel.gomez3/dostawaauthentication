package co.com.pragma.dto;



import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRq {


    private String username;


    private String password;

    private long role;

    private String firstName;


    private String lastName;

    private String email;

}
